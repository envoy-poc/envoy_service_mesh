# envoy_service_mesh

Envoy configurations and complete service mesh. Integration with java-control-plane(management server).

In this example we have 6 docker containers :
1. Envoy_front_proxy
2. Service_1(envoy side car)
3. Service_2(envoy side car)
4. Service_3(envoy side car)
5. Service_4(envoy side car)
6. server(java control plane)

In the above example, envoy front proxy has two clusters, each cluster has two endpoints(two envoy side car services)

Envoy_front_proxy and all envoy side car services have dynamic configurations which are fetched from the server(java control plane) via ADS.

On envoy side car calling ADS will get its endpoints service address along with the external service endpoints.

![Envoy Service Mesh](Envoy_Service_Mesh.png)


Start all containers of service envoy_service_mesh
----

Ensure that you have a recent version of the docker, docker-compose, and docker-machine installed.

1. Move to envoy folder

2. Run the run project with the following command

        $ docker-compose up --build -d

3. To view all running containers

        $ docker-compose ps

4. To stop all containers

        $ docker-compose stop

5. To stop a specific container

        $ docker-compose stop <container-name>

6. To remove all containers
  Note: all containers should be stopped before removing it

        $ docker-compose rm

7. To remove the specific container
  Note: specific container should be stopped before removing it

        $ docker-compose rm <container-name>

Java control plane
------
Please find the below management server code using Java
https://gitlab.com/ideacrestsolutions-poc/envoy_service_mesh/blob/master/java-control-plane/

Building the management server
-----

Build management server fat jar by running in the java-control-plane directory.

      'mvn clean package'

copy server-0.1.15-SNAPSHOT-fat-tests.jar from java-control-plane/server/target/ to https://gitlab.com/ideacrestsolutions-poc/envoy_service_mesh/blob/master/envoy/

The above jar reads the dynamic configuration from  https://gitlab.com/ideacrestsolutions-poc/envoy_service_mesh/blob/master/envoy/dynamic_config.json for every 5 mins.

To change the dynamic configurations
-----
Update the configuration and change the version. The management server's schedule will pick up the node's configuration changes and updates to its respective envoy node.


https://gitlab.com/ideacrestsolutions-poc/envoy_service_mesh/blob/master/envoy/front-envoy.yaml has sample ADS dynamic configuration of envoy front proxy.

Please refer the below doc for Envoy ADS configuration and communication logs: https://docs.google.com/document/d/1MbWIizAttL5d7_g4R-VGPIVw44R3FvyG2WJYEY4_WsQ/edit?usp=sharing   
