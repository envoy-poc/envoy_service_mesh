# envoy-e-commere-example
Objective
---
To load balance traffic between multiple services. Should be able to load balance with weights, as to achieve blue-green deployment. Addition or removal of services should be reflected without stopping or restarting any envoy proxies. Design a basic service mesh with front-proxy that handles traffic from the outside world and load balances traffic to the respective sidecar. Envoy sidecar must load balance and handle resilience strategies for all internal calls and also handle outbound traffic for each service instance, thus providing a coherent layer for observability.

Use Case
---
Consider an E-Commerce application with three services.
Product Service
Cart Service
Payment Service
We need a front load balancer which will route traffic to the different clusters of the above services. Each service instance is associated with Envoy sidecar. ENdpoints available in above services are

    Product Service( /product/* ):
    ---
    Add a product
    Get a product
    Add product to cart

    Cart Service( /cart/* ):
    ----
    Get cart details
    Add to cart
    Clear cart
    Checkout

    Payment Service( /payment/* ):
    ----
    Get payment details
    Add payment details
    Make payment

Envoy- Service mesh
---
 Envoy has Listeners who listen to incoming connections. Listeners have Routes which map to Clusters and Clusters have Endpoints which envoy can finally proxy to. A Service Mesh uses sidecars to handle outbound traffic for each service instance. This allows Envoy to handle load balancing and resilience strategies for all internal calls, as well as providing a coherent layer for observability.

Envoy configuration
---
We have an envoy front proxy with a listener exposed to the outside world on port 8080 that load balances the traffic to the services cluster based on route configurations. There are two instances for each service, where each service instance is bind with its sidecar. Each sidecar has two listeners., ingress_http(handle inbound traffic) and egress_http(handle outbound traffic). Services are still exposed to the internal network, and all network calls pass through an Envoy sidecar on localhost.

Management Server
----
Envoy provides a set of management APIs that can be implemented by control plane services. If a control plane implements all of the APIs, it becomes possible to run Envoy across an entire infrastructure using a generic common bootstrap configuration. All further configuration changes are delivered seamlessly and dynamically via the management server in such a way that Envoy never needs to be restarted. This makes Envoy a universal data plane.
Java-control-plane has a dynamic configuration file(dynamic_config.json) whenever any changes in the configuration file will update it to the management server. Then management server will update the changes to the respective envoy node.

Code Configurations
----
[Envoy-e-commere-example](https://gitlab.com/ideacrestsolutions-poc/envoy_service_mesh/tree/master/envoy-e-commere-example) has the code configuration with the above use case.

Steps to build docker machine and start-up service mesh are

  1. Create a virtual box

        docker-machine create --driver virtualbox default

  2. Make the above environment as default virtual machine

        eval $(docker-machine env default)

  3. Build the docker-compose images

        docker-compose up --build -d

  4. Commands to view running service:

        sudo docker-compose ps

docker-compose commands to view logs:
---
    	sudo docker-compose logs  - to view all logs of envoymesh container
    	sudo docker-compose logs <service_name> - to view service specific logs
    	(eg: sudo docker-compose logs front-envoy)

docker-compose commands to build service instance:
---
      sudo docker-compose up --build -d - start all instances mention in docker-compose.yml
      sudo docker-compose up --build -d <specific service> - starts/build specific service

docker-compose commands to stop service instance:
----
      sudo docker-compose stop - to stop all services
      sudo docker-compose stop <service_name> - to stop specific service

docker-compose commands to remove service instance:
----
      sudo docker-compose rm - to remove all service instances
      sudo docker-compose rm <service_name> - to remove specific service instance
