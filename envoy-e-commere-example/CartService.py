#!/usr/bin/env python3
from flask import Flask
from flask import request
import os
import requests
import socket
import sys

app = Flask(__name__)

TRACE_HEADERS_TO_PROPAGATE = [
    'X-Ot-Span-Context',
    'X-Request-Id',

    # Zipkin headers
    'X-B3-TraceId',
    'X-B3-SpanId',
    'X-B3-ParentSpanId',
    'X-B3-Sampled',
    'X-B3-Flags',

    # Jaeger header (for native client)
    "uber-trace-id"
]

cartData = []

@app.route('/')
def healthcheck():
    return ('Success')

@app.route('/cart/add/<cartname>')
def add(cartname):
    cartData.append(cartname)
    return ('{} added to cart successfully, resolved by host {}.\n'.format(cartname, socket.gethostname()));

@app.route('/cart/get')
def get():
    return ('{}, resolved by host {}.\n'.format(cartData, socket.gethostname()));

@app.route('/cart/clear')
def clear():
    cartData.clear()
    return ('cart cleared successfully, resolved by host {}.\n'.format(socket.gethostname()));

@app.route('/cart/checkout')
def checkout():
    resp = requests.get('http://127.0.0.1:90/payment/add-details/{}'.format(cartData))
    return ('{}\n'.format(resp.text));

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
