package io.envoyproxy.controlplane.server.beans;

import java.util.List;

public class RouteConfig {

  private String routeName;

  private String serviceName;

  private String domain;

  private List<Routes> routesList;

  public String getRouteName() {
    return routeName;
  }

  public void setRouteName(String routeName) {
    this.routeName = routeName;
  }

  public String getServiceName() {
    return serviceName;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  public String getDomain() {
    return domain;
  }

  public void setDomain(String domain) {
    this.domain = domain;
  }

  public List<Routes> getRoutesList() {
    return routesList;
  }

  public void setRoutesList(List<Routes> routesList) {
    this.routesList = routesList;
  }
}
