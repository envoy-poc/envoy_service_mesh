package io.envoyproxy.controlplane.server.scheduler;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Date;

public class SchedulerHelper implements Runnable {

  String cornExp;

  public SchedulerHelper(String cornExp) {
    this.cornExp = cornExp;
  }

  @Override
  public void run() {
    try {
      // First we must get a reference to a scheduler
      SchedulerFactory sf = new StdSchedulerFactory();
      Scheduler sched = sf.getScheduler();

      JobDetail job1 = JobBuilder.newJob(FetchDynamicConfigurationScheduler.class)
          .withIdentity("Scheduler1", "group1")
          .build();

      //This trigger will run every minute in infinite loop
      Trigger trigger1 = TriggerBuilder.newTrigger()
          .withIdentity("everyFiveMinuteTrigger", "group1")
          .withSchedule(CronScheduleBuilder.cronSchedule(cornExp))
          .build();

      Date ft = sched.scheduleJob(job1, trigger1);
      sched.start();
      sched.triggerJob(job1.getKey());
      System.out.println(job1.getKey() + " has been scheduled to run at: " + ft);

      Thread.sleep(Long.MAX_VALUE);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
