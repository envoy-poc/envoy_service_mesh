package io.envoyproxy.controlplane.server.beans;

public class WeightedCluster {

  private String clusterName;

  private int weight;

  public String getClusterName() {
    return clusterName;
  }

  public void setClusterName(String clusterName) {
    this.clusterName = clusterName;
  }

  public int getWeight() {
    return weight;
  }

  public void setWeight(int weight) {
    this.weight = weight;
  }
}
