package io.envoyproxy.controlplane.server.scheduler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import io.envoyproxy.controlplane.cache.Snapshot;
import io.envoyproxy.controlplane.server.CacheManagement;
import io.envoyproxy.controlplane.server.beans.DynamicADSConfiguration;
import io.envoyproxy.envoy.api.v2.Cluster;
import io.envoyproxy.envoy.api.v2.ClusterLoadAssignment;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FetchDynamicConfigurationScheduler implements Job {

  public static int hashVal = 0;

  public FetchDynamicConfigurationScheduler() {
  }

  @Override
  public void execute(JobExecutionContext jobExecutionContext) {

    System.out.println("Reading the dynamic configuration at " + new Date(System.currentTimeMillis()));

    try {

      String dynamicData = new String(Files.readAllBytes(Paths.get(CacheManagement.fileName)));

      System.out.println(dynamicData.hashCode() + " ---- " + hashVal);

      if (dynamicData.hashCode() != hashVal) {

        List<DynamicADSConfiguration> dynamicADSConfigurationList = CacheManagement.mapper.readValue(
            dynamicData, new TypeReference<List<DynamicADSConfiguration>>() {
            });

        for (DynamicADSConfiguration eachDynamicADSConfiguration : dynamicADSConfigurationList) {

          // initialize clusters
          List<Cluster> clusters = eachDynamicADSConfiguration.getClusterNameList().stream()
              .map(CacheManagement::getClusterBuild).collect(Collectors.toList());

          // initialize endpoints
          List<ClusterLoadAssignment> endPoints = eachDynamicADSConfiguration.getEndpointConfigList().stream()
              .map(CacheManagement::getClusterLoadAssignmentForFrontProxy).collect(Collectors.toList());

          CacheManagement.cache.setSnapshot(
              eachDynamicADSConfiguration.getNodeClusterName(),
              Snapshot.create(
                  Collections.unmodifiableList(clusters),
                  Collections.unmodifiableList(endPoints),
                  ImmutableList.of(
                      CacheManagement.getListenerBuild(eachDynamicADSConfiguration.getListenerConfig())
                  ),
                  ImmutableList.of(
                      CacheManagement.getRouteConfigurationBuild(eachDynamicADSConfiguration.getRouteConfig())
                  ),
                  ImmutableList.of(),
                  eachDynamicADSConfiguration.getVersion()
                  ));
        }
        System.out.println("Cache updated successfully!!!");
        hashVal = dynamicData.hashCode();
      } else {
        System.out.println("Configuration file is unmodified. Cache remains same !!!");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    System.out.println("Completed the sync of dynamic configuration at " + new Date(System.currentTimeMillis()));

  }

}
