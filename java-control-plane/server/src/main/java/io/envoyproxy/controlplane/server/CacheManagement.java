package io.envoyproxy.controlplane.server;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.*;
import com.google.protobuf.util.JsonFormat;
import io.envoyproxy.controlplane.cache.SimpleCache;
import io.envoyproxy.controlplane.server.beans.*;
import io.envoyproxy.envoy.api.v2.Cluster;
import io.envoyproxy.envoy.api.v2.ClusterLoadAssignment;
import io.envoyproxy.envoy.api.v2.Listener;
import io.envoyproxy.envoy.api.v2.RouteConfiguration;
import io.envoyproxy.envoy.api.v2.core.*;
import io.envoyproxy.envoy.api.v2.endpoint.Endpoint;
import io.envoyproxy.envoy.api.v2.endpoint.LbEndpoint;
import io.envoyproxy.envoy.api.v2.endpoint.LocalityLbEndpoints;
import io.envoyproxy.envoy.api.v2.listener.Filter;
import io.envoyproxy.envoy.api.v2.listener.FilterChain;
import io.envoyproxy.envoy.api.v2.route.*;
import io.envoyproxy.envoy.api.v2.route.WeightedCluster;
import io.envoyproxy.envoy.config.filter.network.http_connection_manager.v2.HttpConnectionManager;
import io.envoyproxy.envoy.config.filter.network.http_connection_manager.v2.HttpFilter;
import io.envoyproxy.envoy.config.filter.network.http_connection_manager.v2.Rds;

public class CacheManagement {

  public static final String ENVOY_FRONT_PROXY_GROUP = "envoy_front_proxy";

  public static String fileName;

  public static ObjectMapper mapper = new ObjectMapper();


  public static SimpleCache<String> cache = new SimpleCache<>(node -> {
    if (node == null || node.getCluster() == null) {
      return ENVOY_FRONT_PROXY_GROUP;
    }
    return node.getCluster();
  });

  public static Listener getListenerBuild(ListenerConfig listenerConfig) {
    return Listener.newBuilder()
        .setName(listenerConfig.getListnerName())
        .setAddress(getAddressBuild(listenerConfig.getIpAddress(), listenerConfig.getPort())
        )
        .addFilterChains(FilterChain.newBuilder()
            .addFilters(Filter.newBuilder()
                .setName("envoy.http_connection_manager")
                .setConfig(messageAsStruct(getHttpConnectionManager()))
                .build()
            ).build()
        ).build();
  }

  public static RouteConfiguration getRouteConfigurationBuild(RouteConfig routeConfig) {

    VirtualHost.Builder builder = VirtualHost.newBuilder();
    builder.setName(routeConfig.getServiceName());
    builder.addDomains(routeConfig.getDomain());

    for (Routes routes : routeConfig.getRoutesList()) {

      if (routes.getWeightedClusterList() != null) {

        WeightedCluster.Builder weightedBuilder = WeightedCluster.newBuilder();

        for (io.envoyproxy.controlplane.server.beans.WeightedCluster eachWeightedCluster : routes.getWeightedClusterList()) {
          weightedBuilder.addClusters(WeightedCluster.ClusterWeight.newBuilder()
              .setName(eachWeightedCluster.getClusterName())
              .setWeight(UInt32Value.newBuilder().setValue(eachWeightedCluster.getWeight()).build())
              .build());
        }

        builder.addRoutes(Route.newBuilder()
            .setMatch(RouteMatch.newBuilder()
                .setPrefix(routes.getRoutePrefix())
                .build())
            .setRoute(RouteAction.newBuilder()
                .setWeightedClusters(weightedBuilder.build())
                .build())
            .build());

      } else {

        builder.addRoutes(Route.newBuilder()
            .setMatch(RouteMatch.newBuilder()
                .setPrefix(routes.getRoutePrefix())
                .build())
            .setRoute(RouteAction.newBuilder()
                .setCluster(routes.getClusterName())
                .build())
            .build());
      }
    }

    return RouteConfiguration.newBuilder()
        .setName(routeConfig.getRouteName())
        .addVirtualHosts(builder.build())
        .build();
  }


  public static Cluster getClusterBuild(String clusterName) {
    return Cluster.newBuilder()
        .setName(clusterName)
        .setConnectTimeout(Duration.newBuilder().setSeconds(10))
        .setLbPolicy(Cluster.LbPolicy.ROUND_ROBIN)
        .setType(Cluster.DiscoveryType.EDS)
        .setEdsClusterConfig(Cluster.EdsClusterConfig.newBuilder()
            .setEdsConfig(ConfigSource.newBuilder()
                .setAds(AggregatedConfigSource.newBuilder().build())
            ).build())
        .build();
  }

  public static ClusterLoadAssignment getClusterLoadAssignmentForFrontProxy(EndpointConfig endpointConfig) {

    LocalityLbEndpoints.Builder builder = LocalityLbEndpoints.newBuilder();

    for (LBPoints eachLbPoints : endpointConfig.getLbPointsList()) {
      builder.addLbEndpoints(LbEndpoint.newBuilder()
          .setEndpoint(Endpoint.newBuilder()
              .setAddress(Address.newBuilder()
                  .setSocketAddress(SocketAddress.newBuilder()
                      .setAddress(eachLbPoints.getIpAddress())
                      .setPortValue(eachLbPoints.getPort())
                      .build()
                  ).build()
              ).build())
          .build());
    }

    return ClusterLoadAssignment.newBuilder()
        .setClusterName(endpointConfig.getClusterName()).addEndpoints(builder.build()).build();
  }

  private static Address getAddressBuild(String ip, int port) {
    return Address.newBuilder()
        .setSocketAddress(
            SocketAddress.newBuilder()
                .setAddress(ip)
                .setPortValue(port)
        ).build();
  }

  private static HttpConnectionManager getHttpConnectionManager() {
    return
        HttpConnectionManager.newBuilder()
            .setStatPrefixBytes(ByteString.copyFrom("ingress_http".getBytes()))
            .setCodecType(HttpConnectionManager.CodecType.AUTO)
            .setRds(Rds.newBuilder()
                .setRouteConfigName("local_route")
                .setConfigSource(ConfigSource.newBuilder()
                    .setApiConfigSource(ApiConfigSource.newBuilder()
                        .setApiType(ApiConfigSource.ApiType.GRPC)
                        .addGrpcServices(GrpcService.newBuilder()
                            .setEnvoyGrpc(GrpcService.EnvoyGrpc.newBuilder()
                                .setClusterName("ads_cluster")
                                .build()
                            ).build()
                        ).build()
                    ).build()
                ).build()
            )
            .addHttpFilters(HttpFilter.newBuilder()
                .setName("envoy.router")
                .build()
            )
            .build();
  }

  private static Struct messageAsStruct(MessageOrBuilder message) {
    try {

      String json = JsonFormat.printer()
          .preservingProtoFieldNames()
          .print(message);

      Struct.Builder structBuilder = Struct.newBuilder();

      JsonFormat.parser().merge(json, structBuilder);

      return structBuilder.build();

    } catch (Exception e) {
      throw new RuntimeException("Failed to convert protobuf message to struct", e);
    }
  }
}
