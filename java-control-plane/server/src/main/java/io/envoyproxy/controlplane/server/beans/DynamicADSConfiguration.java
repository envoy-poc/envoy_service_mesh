package io.envoyproxy.controlplane.server.beans;

import java.util.List;

public class DynamicADSConfiguration {

  private String nodeClusterName;

  private ListenerConfig listenerConfig;

  private RouteConfig routeConfig;

  private List<String> clusterNameList;

  private List<EndpointConfig> endpointConfigList;

  private String version;

  public String getNodeClusterName() {
    return nodeClusterName;
  }

  public void setNodeClusterName(String nodeClusterName) {
    this.nodeClusterName = nodeClusterName;
  }

  public ListenerConfig getListenerConfig() {
    return listenerConfig;
  }

  public void setListenerConfig(ListenerConfig listenerConfig) {
    this.listenerConfig = listenerConfig;
  }

  public RouteConfig getRouteConfig() {
    return routeConfig;
  }

  public void setRouteConfig(RouteConfig routeConfig) {
    this.routeConfig = routeConfig;
  }

  public List<String> getClusterNameList() {
    return clusterNameList;
  }

  public void setClusterNameList(List<String> clusterNameList) {
    this.clusterNameList = clusterNameList;
  }

  public List<EndpointConfig> getEndpointConfigList() {
    return endpointConfigList;
  }

  public void setEndpointConfigList(List<EndpointConfig> endpointConfigList) {
    this.endpointConfigList = endpointConfigList;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

}
