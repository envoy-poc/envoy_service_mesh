package io.envoyproxy.controlplane.server.beans;

public class ListenerConfig {

  private String listnerName;

  private String ipAddress;

  private int port;

  public String getListnerName() {
    return listnerName;
  }

  public void setListnerName(String listnerName) {
    this.listnerName = listnerName;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  public int getPort() {
    return port;
  }

  public void setPort(int port) {
    this.port = port;
  }
}
