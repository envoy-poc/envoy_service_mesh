package io.envoyproxy.controlplane.server.beans;

import java.util.List;

public class Routes {

  private String routePrefix;

  private String clusterName;

  private List<WeightedCluster> weightedClusterList;

  public String getRoutePrefix() {
    return routePrefix;
  }

  public void setRoutePrefix(String routePrefix) {
    this.routePrefix = routePrefix;
  }

  public String getClusterName() {
    return clusterName;
  }

  public void setClusterName(String clusterName) {
    this.clusterName = clusterName;
  }

  public List<WeightedCluster> getWeightedClusterList() {
    return weightedClusterList;
  }

  public void setWeightedClusterList(List<WeightedCluster> weightedClusterList) {
    this.weightedClusterList = weightedClusterList;
  }
}
