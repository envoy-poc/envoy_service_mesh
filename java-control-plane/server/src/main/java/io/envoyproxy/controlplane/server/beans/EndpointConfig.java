package io.envoyproxy.controlplane.server.beans;

import java.util.List;

public class EndpointConfig {

  private String clusterName;

  private List<LBPoints> lbPointsList;

  public String getClusterName() {
    return clusterName;
  }

  public void setClusterName(String clusterName) {
    this.clusterName = clusterName;
  }

  public List<LBPoints> getLbPointsList() {
    return lbPointsList;
  }

  public void setLbPointsList(List<LBPoints> lbPointsList) {
    this.lbPointsList = lbPointsList;
  }
}
