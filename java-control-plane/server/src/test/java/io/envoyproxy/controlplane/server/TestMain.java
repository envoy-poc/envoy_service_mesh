package io.envoyproxy.controlplane.server;

import io.envoyproxy.controlplane.server.scheduler.FetchDynamicConfigurationScheduler;
import io.envoyproxy.controlplane.server.scheduler.SchedulerHelper;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.netty.NettyServerBuilder;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.io.IOException;
import java.util.Date;

public class TestMain {


  /**
   * Example minimal xDS implementation using the java-control-plane lib.
   *
   * @param arg command-line args
   */
  public static void main(String[] arg) throws IOException, InterruptedException {

    CacheManagement.fileName = arg[0];

    DiscoveryServer discoveryServer = new DiscoveryServer(CacheManagement.cache);

    ServerBuilder builder = NettyServerBuilder.forPort(5678)
        .addService(discoveryServer.getAggregatedDiscoveryServiceImpl());

    Server server = builder.build();

    server.start();

    System.out.println("Server has started on port " + server.getPort());

    Thread schedulerThread = new Thread(new SchedulerHelper(arg[1]));
    schedulerThread.start();

    Runtime.getRuntime().addShutdownHook(new Thread(server::shutdown));

    server.awaitTermination();
  }
}
